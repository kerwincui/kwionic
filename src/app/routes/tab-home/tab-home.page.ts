import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Storage } from '@ionic/storage';
import { CommonService } from 'src/app/core/common/common.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-tab-home',
  templateUrl: 'tab-home.page.html',
  styleUrls: ['tab-home.page.scss']
})
export class TabHomePage implements OnInit{

  constructor(
    private router: Router,
    private storage: Storage,
    private activatedRoute: ActivatedRoute,
    private http: HttpClient,
    private commonService:CommonService) {}

  companyName='Ionic5 demo';
  slideOpts = {initialSlide: 1,speed: 400}

  imageList = [
    { src: 'assets/icon/img.png' },
    { src: 'assets/icon/img.png' },
    { src: 'assets/icon/img.png' },
    { src: 'assets/icon/img.png' }]

  ngOnInit(){
    this.activatedRoute.params.subscribe(params=>{
      this.getCompanyInfo();
    })

    // 判断是否登录
    this.commonService.getToken().then((val) => {
      if(val==null){
        this.router.navigate(['login']);
      }
    });

  }

  /**
   * 获取公司信息
   */
  getCompanyInfo(){
    this.commonService.getToken().then((token) => {
      const headerOptions = new HttpHeaders().set('Content-Type', 'application/json; charset=UTF-8')
        .set('Authorization', 'Bearer ' + token);
      this.http.get('/api/home/get' , { headers: headerOptions })
        .subscribe((res: any) => {
          this.companyName=res.company;
        });
    })
  }

  logout() {
    this.storage.remove('token');
    this.router.navigate(['login']);
  }

  goDemo() {
    this.router.navigate(['/tabs/demo'])
  }

  goCenter() {
    this.router.navigate(['/tabs/center'])
  }
}
