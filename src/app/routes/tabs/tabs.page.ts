import { Component, OnInit, ViewChildren, QueryList } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Router, RouterOutlet } from '@angular/router';
import { Platform, AlertController, IonRouterOutlet } from '@ionic/angular';
import { Plugins } from '@capacitor/core';
import { CommonService } from 'src/app/core/common/common.service';
const { App } = Plugins;

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})


export class TabsPage implements OnInit {

  constructor(private router: Router,
              private alertController: AlertController,
              private commonService: CommonService,
              private platform: Platform) {
    this.backButtonEvent();
  }


  ngOnInit() {
  }

  /**
   * 跳转运单
   */
  goWayBill(){
    this.router.navigate(['/tabs/way-bill/0']);
  }

  /**
   * 退出按钮事件
   */
  backButtonEvent() {
    this.platform.backButton.subscribe(() => {
      if (this.router.url === '/tabs/home') {
          this.presentAlertConfirm();
      }else{
        this.router.navigate(['/tabs/home']);
      };
    });
  }

  /**
   * 确认退出
   */
  async presentAlertConfirm() {
    const alert = await this.alertController.create({
      header: '提醒',
      message: '确定要退出APP吗？',
      buttons: [{
        text: '取消',
        role: 'cancel',
        cssClass: 'secondary',
        handler: (blah) => {}
      }, {
        text: '确定',
        handler: () => {
          navigator['app'].exitApp();
        }
      }]
    });
    await alert.present();
  }

}
