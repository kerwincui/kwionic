import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CommonService } from 'src/app/core/common/common.service';
import { AlertController, ModalController } from '@ionic/angular';
import { DemoModalPage } from './model-page/demo-modal-page';

@Component({
  selector: 'app-receipt-delivery-info',
  templateUrl: './demo.component.html',
  styleUrls: ['./demo.component.scss'],
})
export class DemoComponent implements OnInit {

  constructor(
    private http: HttpClient,
    private commonService: CommonService,
    private alertController: AlertController,
    public modalController: ModalController) { }

  title='示例';
  list = [];
  paging = { pi: 1, ps: 10, isOver: false, total: 0 }; // 分页
  search = { companyContactPeople: ''}; // 查询条件
  idList: number[]=[];
  isNoData=false;

  ngOnInit() {
    this.getList();
  }

  /**
   * 获取列表数据
   */
  async getList(isAdd:boolean=false) {
    if(!isAdd) this.paging.pi=1;
    const condition=`?companyContactPeople=${this.search.companyContactPeople}&pi=${this.paging.pi}&ps=${this.paging.ps}`;
      const headerOptions=new HttpHeaders().set('Content-Type','application/json; charset=UTF-8')
      .set('Authorization','Bearer '+await this.commonService.getToken());
      this.http.get('/api/地址'+condition,{headers:headerOptions})
      .subscribe((res: any) => {
        this.paging.total = res.total;
        if(this.paging.total===0){
          this.isNoData=true;
        }else{
          this.isNoData=false;
        }
        if(isAdd){
          res.data.forEach(element => {
            this.list.push(element);
          });
        }else{
          this.list = res.data;
        }
      });
  }

  /**
   * 查询
   */
  onSearch(){
    this.getList();
  }

  /**
   * 删除提醒
   */
  async presentAlertConfirm(id:number) {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: '警告',
      message: '确定要删除吗？',
      buttons: [
        {
          text: '取消',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
          }
        }, {
          text: '确定',
          handler: () => {
            const body=`{"idList":[${id}]}`
            this.commonService.getToken().then((token) =>{
              const headerOptions=new HttpHeaders().set('Content-Type','application/json; charset=UTF-8')
              .set('Authorization','Bearer '+token);
              this.http.post('/api/地址',body,{headers:headerOptions})
              .subscribe((res: any) => {
                if (res.code === 1) {
                  this.getList();
                  this.commonService.presentToast('删除成功');
                } else {
                  this.commonService.presentToast(res.message);
                }
              });
            })
          }
        }
      ]
    });
    await alert.present();
  }

  /**
   * 删除
   */
  onDelete(id: number){
    this.presentAlertConfirm(id);
  }

  /**
   * 编辑
   */
  onEdit(id: number){
    this.presentModal(id,false);
  }

  /**
   * 查看
   */
  onDetail(id: number){
    this.presentModal(id,true);
  }

  /**
   * 上拉加载数据
   */
  loadData(event) {
    setTimeout(() => {
      event.target.complete();
      this.paging.pi+=1;
      this.getList(true);
    }, 1000);
  }

  /**
   * 下拉刷新
   */
  doRefresh(event) {
    this.getList();
    setTimeout(() => {
      event.target.complete();
    }, 500);
  }

  // ----------------------------------------编辑功能------------------------------------------------------

  /**
   * 打开模态
   */
  async presentModal(id: number,isViewMode) {
    const modal = await this.modalController.create({
      component: DemoModalPage,
      cssClass: 'my-custom-class',
      componentProps: {id,isViewMode}
    });
    await modal.present();
    await modal.onDidDismiss();
    this.getList();
  }




}
