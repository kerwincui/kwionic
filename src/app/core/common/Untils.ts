/**
 * Created by yanxiaojun617@163.com on 12-27.
 */
import { Injectable } from '@angular/core';
import * as JsEncryptModule from 'jsencrypt';
/**
 * Utils类存放和业务无关的公共方法
 * @description
 */
@Injectable()
export class Utils {
  // 获取加密后的字符串，必须是字符串类型的

  static publicKey = 'MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAyIPUVOxaYsl+s1ABQzJp1LDQ4pdSNeF9LqRnIOrIQjwab8WtbA9e+6CZ1/pF247768Y2KpBgOSXn3wLHvsoIDsy6ory9LxSHp77OTD2V+Zdya0PfwL6NJuxJ7UDXc/hG3w6wLEz3VNHDb9v3nNO9WZtdrCJccRd0B+MIq4mmyjEViCVU232vYbCf07yyYAa+8cwoHaTpD3v5yesBWQK5aWwfnV6Xd90SgjVry1Sj1AhtHkAQWkacXQFI4cLM1dksBj2F1N7DafHR3vCxpQRYEm4WN++e6+B5yRIcn4w8/CXh+3q7f6rEkcA8qQAAKI9Me+PICn5gh53WYKt7IpE2PQIDAQAB';

  static getkeys(data) {
    const encrypt = new JsEncryptModule.JSEncrypt();
    encrypt.setPublicKey(this.publicKey);
    const sendData = new Object();
    // tslint:disable-next-line: forin
    for (const key in data) {
      sendData[key] = encrypt.encrypt(data[key]);
    }
    return sendData;
  }
  /** 加密单个参数 */
  static getkey(Value) {
    const encrypt = new JsEncryptModule.JSEncrypt();
    encrypt.setPublicKey(this.publicKey);
    return encrypt.encrypt(Value);
  }

}

