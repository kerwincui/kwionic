
import { NgModule } from '@angular/core';
import { ImgUploadComponent } from './img-upload/img-upload.component';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';


const COMPONENTS = [ImgUploadComponent];

@NgModule({
  declarations: [...COMPONENTS],
  imports: [
    CommonModule,IonicModule
  ],
  exports: [...COMPONENTS]
})
export class ComponentsModule { }
