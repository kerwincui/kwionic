import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { LoginComponent } from './routes//login/login.component';
import { DemoComponent } from './routes/demo/demo.component';
import { DemoModalPage } from './routes/demo/model-page/demo-modal-page';
import { UserCenterComponent } from './routes/user-center/user-center.component';


const routes: Routes = [
    { path: 'login', component: LoginComponent },
    { path: '', loadChildren: () => import('./routes/tabs/tabs.module').then(m => m.TabsPageModule) }

];
@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  declarations: [
    LoginComponent,
    DemoComponent,
    DemoModalPage,
    UserCenterComponent],
  exports: [RouterModule]
})
export class AppRoutingModule {}
